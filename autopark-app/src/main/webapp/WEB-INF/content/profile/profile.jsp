<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="../tiles/head.jsp" %>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="/js/date.js"></script>
    <link rel="stylesheet" type="text/css" href="/styles/profile.css" />
</head>
<body>

<tags:headerV2 activeProfile="active"/>
    <div id="wrap">

        <div class="container">

        <tags:profilebar activeProfile="active"/>


        <form class="form-profile">
            <div class="form-group">
                <label for="ID">ID пользователя в системе:</label>
                <input name="id" type="text" readonly="readonly" class="form-control" id="ID" placeholder="ID" value="${command.id}">

                <c:if test="${command.myPhotoImageFileId!=null}">
                    <img src="/imf/${command.myPhotoImageFileId}" width="54" height="54" id='userPhoto' class=""/>
                </c:if>
            </div>
            <div class="form-group">
                <label for="NAME">Имя:</label>
                <input name="name" type="text"  class="form-control" id="NAME" placeholder="Имя" value="${command.name}">
            </div>

            <div class="form-group">
                <label for="EMAIL">Email:</label>
                <input name="email" type="text"  class="form-control" id="EMAIL" placeholder="Email" value="${command.email}">
            </div>

            <div class="form-group">
                <label for="PHONE">Телефон:</label>
                <input name="phone" type="text" class="form-control" id="PHONE" placeholder="Телефон" value="${command.phone}">
            </div>

            <div class="form-group">
                <label for="BIRTHDAY">Дата рождения:</label>
                <input name="birthday" type="text"  class="form-control datepicker" id="BIRTHDAY" placeholder="Дата рождения" autocomplete="off"
                       value='<fmt:formatDate value="${command.birthday}" pattern="dd.MM.yyyy" />'>
            </div>

            <div class="form-group">
                <label for="CRDATE">Дата создания:</label>
                <input name="creationDate" type="text" readonly="readonly" class="form-control" id="CRDATE" placeholder="Дата создания"
                       value='<fmt:formatDate value="${command.creationDate}" pattern="dd.MM.yyyy hh:mm:ss" />'>
            </div>


            <button type="submit" class="btn btn-default">Сохранить</button>
        </form>

        </div>


    </div>


    <hr>
    <%@ include file="../tiles/footer.jsp" %>


</body>
</html>
