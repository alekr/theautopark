$(document).ready(function() {
    $.i18n.properties({
        name: 'message',
        path: 'resources/',
        encoding: 'UTF-8',
        mode: 'both'
    });
    //load vendors into drop down
    $.getJSON("/dic/vendors", function(result) {
        var selectbox = $("#sel-vendor");
        $.each(result, function() {
            selectbox.append($("<option />").val(this).text(this));
        });
        selectItem(selectbox);
    });

    //load car types into drop down
    $.getJSON("/dic/types", function(result) {
        var selectbox = $("#sel-type");
        $.each(result, function() {
            selectbox.append($("<option />").val(this.name).text(this.displayName));
        });
        selectItem(selectbox);
    });

    //load car types into drop down
    $.getJSON("/dic/capacity", function(result) {
        var selectbox = $("#sel-capacity");
        $.each(result, function() {
            selectbox.append($("<option />").val(this).text($.i18n.prop(this+".displayName")));
        });
        selectItem(selectbox);
    });

    selectItem = function(selectbox){
        var selectedItem = $(selectbox).closest('div.form-group').attr("title");
        if(selectedItem){
            $(selectbox).val(selectedItem);
        }
    };

    $( "#clear-filter" ).click(function() {
        $("#sel-capacity").val("0");
        $("#sel-type").val("0");
        $("#sel-vendor").val("0");
        $("input[name='searchText']").val("");
        $( "form" ).submit();
    });
});

