package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.service.ICarService;
import autopark.service.IWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Comparator;
import java.util.List;

@Controller
public class MainPageController {

    @Autowired
    @Qualifier("carServiceHibernate")
    private ICarService carServiceHibernate;

    @Autowired
    @Qualifier("carServiceJdbc")
    private ICarService carServiceJdbc;

    @Autowired
    private IWebService webService;


    @RequestMapping(value = {"/main","/"})
    public String handle(ModelMap modelMap) {
        return handleRequest(modelMap);
    }



    private String handleRequest(ModelMap modelMap){
        modelMap.put("cars",carServiceJdbc.getCars());
        modelMap.put("thread","JDBC");
        modelMap.put("author",System.getProperty("user.name"));
        return "/WEB-INF/content/main.jsp";
    }


    private class ThreadResult{
        String thread;
        List<CarDTO> list;
    }

    private class CarComparator implements Comparator<CarDTO>{
        @Override
        public int compare(CarDTO o1, CarDTO o2) {
            if(o1.getVendor() !=null && o2.getVendor() != null){
                if(o1.getVendor().name().equals(o2.getVendor().name())){
                    return o2.getId().compareTo(o1.getId());
                }
                return o1.getVendor().name().compareTo(o2.getVendor().name());
            }else{
                return -1;
            }
        }
    }


}



