package autopark.dao;

import autopark.domain.PasswordResetToken;

/**
 * 21.03.2016.
 */
public interface IPasswordResetTokenDAO extends IRootDAO<PasswordResetToken>{

    PasswordResetToken findOneByTokenValue(String tokenValue);

    PasswordResetToken findOneByEmail(String email);

    void delete(PasswordResetToken token);

}
