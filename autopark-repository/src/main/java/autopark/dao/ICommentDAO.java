package autopark.dao;


import autopark.domain.Comment;
import autopark.domain.CommentEntityEnum;

import java.util.List;

public interface ICommentDAO extends IRootDAO<Comment> {

    List<Comment> findComments(CommentEntityEnum entityEnum, Long id);
}