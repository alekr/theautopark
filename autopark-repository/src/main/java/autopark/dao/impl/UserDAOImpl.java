package autopark.dao.impl;

import autopark.dao.IUserDAO;
import autopark.domain.User;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Repository
public class UserDAOImpl extends RootDAOImpl<User> implements IUserDAO {
    private static final String GET_BY_EMAIL = "from User where email = ?";
    private static final String GET_BY_FACEBOOK_ID = "from User where facebookId = ?";
    private static final String GET_BY_PHONE = "from User where phone = ? order by creationDate Desc";

    public UserDAOImpl() {
        super("autopark.domain.User", User.class);
    }

    public User getByEmail(final String email) {
        if(StringUtils.isEmpty(email)) return null;
        final Query query = getSession().createQuery(GET_BY_EMAIL).setString(0, email);
        List<User> list = query.list();
        if(CollectionUtils.isEmpty(list)){
            return null;
        }else{
            return list.get(0);
        }
    }

    public User getByFacebookId(final String facebookId) {
        if(StringUtils.isEmpty(facebookId)) return null;
        final Query query = getSession().createQuery(GET_BY_FACEBOOK_ID).setString(0, facebookId);
        List<User> list = query.list();
        if(CollectionUtils.isEmpty(list)){
            return null;
        }else{
            return list.get(0);
        }
    }

    @Override
    public List<User> getByPhone(final String phone) {
        if(StringUtils.isEmpty(phone)) return null;

        return (List<User>)getHibernateTemplate().find(GET_BY_PHONE,phone);
    }

    public void removeTestUsers(final String phone) {
        getHibernateTemplate().execute(
                new HibernateCallback() {
                    @Override
                    public Object doInHibernate(org.hibernate.Session session) throws HibernateException {
                        SQLQuery query = session.createSQLQuery(" delete from ap_user where phone = ? ");
                        query.setString(0, phone);
                        query.executeUpdate();
                        return null;
                    }
                });
    }

}