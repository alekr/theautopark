package autopark.dao.impl;

import autopark.dao.IRoleUserDAO;
import autopark.domain.RoleUser;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;


@Repository
public class RoleUserDAOImpl extends RootDAOImpl<RoleUser> implements IRoleUserDAO {

    public RoleUserDAOImpl() {
        super("autopark.domain.RoleUser", RoleUser.class);
    }


    public void removeRolesOfTestUsers(final String phone) {
        getHibernateTemplate().execute(
                new HibernateCallback() {
                    @Override
                    public Object doInHibernate(org.hibernate.Session session) throws HibernateException {
                        SQLQuery query = session.createSQLQuery(" delete from ap_role_user where USER_ID in (select id from ap_user where phone = ? ) ");
                        query.setString(0, phone);
                        query.executeUpdate();
                        return null;
                    }
                });
    }

}