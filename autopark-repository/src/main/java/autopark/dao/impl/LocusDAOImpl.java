package autopark.dao.impl;

import autopark.dao.GenericDAO;
import autopark.domain.Locus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 10.04.2016.
 */
@Repository
public class LocusDAOImpl implements ILocusDAO {

    private final Logger log = LoggerFactory.getLogger(LocusDAOImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;




    @Override
    public boolean add(Locus entity) {

        String sql = "insert into ap_locus (lat, lon, date) values (?, ?, ?)";
        try{
            log.info(sql);
            jdbcTemplate.update(sql, entity.getLat(), entity.getLon(), entity.getDate());
        }catch (Exception e){
            log.error(e.getMessage());
            return false;
        }
        return true;

    }

    @Override
    public void saveOrUpdate(Locus entity) {

    }

    @Override
    public void update(Locus entity) {

    }

    @Override
    public void remove(Locus entity) {

    }

    @Override
    public Locus find(Long key) {
        return null;
    }

    @Override
    public List<Locus> getAll() {
        return null;
    }
}
