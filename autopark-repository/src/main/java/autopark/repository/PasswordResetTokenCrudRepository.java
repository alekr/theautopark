package autopark.repository;

import autopark.domain.PasswordResetToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 21.03.2016.
 */
@Repository
public interface PasswordResetTokenCrudRepository extends CrudRepository<PasswordResetToken, Long> {
}
