package autopark.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 10.04.2016.
 */

@Entity
@Table(name = "ap_locus")
public class Locus extends Root{

    private Double lon;

    private Double lat;

    private Date date;

    /*@ManyToOne
    private TrackerUid uid;*/

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /*public TrackerUid getUid() {
        return uid;
    }

    public void setUid(TrackerUid uid) {
        this.uid = uid;
    }*/
}
